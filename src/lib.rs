/*!
# rain-ast

This crate contains the AST and parser for the standard textual representation of the [`rain` intermediate representation](https://gitlab.com/rain-ir).
*/
#![forbid(unsafe_code, missing_docs, missing_debug_implementations)]
#![warn(clippy::all)]

pub mod ast;
pub mod parser;