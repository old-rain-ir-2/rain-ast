/*!
The parser for the `rain` intermediate representation

This converts the standard textual representation for `rain` IR into an AST.
*/
use super::*;
use ast::*;
use nom::branch::*;
use nom::bytes::complete::{is_a, is_not, tag};
use nom::character::{complete::*, *};
use nom::combinator::*;
use nom::multi::*;
use nom::sequence::*;
use nom::IResult;
use num::BigUint;
use std::str;
use std::sync::Arc;

mod util;
pub use util::*;

/// The set of special characters
pub const SPECIAL_CHARACTERS: &str = "\n\r\t []()=#-<>\"\\,;";

/// The keyword for the boolean constant `true`
pub const TRUE: &str = "#true";
/// The keyword for the boolean constant `false`
pub const FALSE: &str = "#false";
/// The keyword for the boolean representation
pub const BOOL_TY: &str = "#bool";
/// The keyword for the type of natural numbers
pub const NATS_TY: &str = "#nats";
/// The keyword for the family of finite types
pub const FINITE_TY: &str = "#finite";
/// The keyword for a polymorphic typing universe
pub const UNIVERSE: &str = "#universe";

/// Parse a `rain` expression
///
/// This parser consumes preceding whitespace
pub fn expr(input: &str) -> IResult<&str, Expr> {
    let expr = alt((
        // == Basic syntax ==
        map(ident, |ident| Expr::Ident(ident.into())),
        map(sexpr, Expr::Sexpr),
        // == Primitive types ==
        map(tag(NATS_TY), |_| Expr::Nats),
        map(natural, Expr::Natural),
        map(tag(FINITE_TY), |_| Expr::Finite),
        map(boolean, Expr::Boolean),
        map(tag(BOOL_TY), |_| Expr::Bool),
        // == Typing universes ==
        universe,
    ));
    preceded(opt(ws), expr)(input)
}

/// Parse a string forming a valid `rain` identifier
///
/// A `rain` identifier may be any sequence of non-whitespace characters which does not
/// contain a special character. This parser does *not* consume preceding whitespace!
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::ident;
/// assert_eq!(ident("hello "), Ok((" ", "hello")));
/// assert!(ident(" bye").is_err());
/// assert!(ident("0x35").is_err());
/// assert_eq!(ident("x35"), Ok(("", "x35")));
/// assert_eq!(ident("你好"), Ok(("", "你好")));
/// let arabic = ident("الحروف العربية").unwrap();
/// let desired_arabic = (" العربية" ,"الحروف");
/// assert_eq!(arabic, desired_arabic);
/// ```
pub fn ident(input: &str) -> IResult<&str, &str> {
    verify(is_not(SPECIAL_CHARACTERS), |ident: &str| {
        !is_digit(ident.as_bytes()[0])
    })(input)
}

/// Parse an S-expression
///
/// An S-expression is a sequence of `rain` identifiers delimited by brackets, and
/// represents curried function application. This parser does not consume preceding whitespace.
///
/// # Examples
/// ```rust
/// # use rain_ast::ast::{Sexpr, Expr::Ident};
/// # use rain_ast::parser::sexpr;
/// assert_eq!(sexpr("()"), Ok(("", Sexpr::NIL)));
/// assert_eq!(
///     sexpr("(f x y)"),
///     Ok(("", Sexpr(vec![
///         Ident("f".to_owned()).into(),
///         Ident("x".to_owned()).into(),
///         Ident("y".to_owned()).into()    
///     ])))
/// )
/// ```
pub fn sexpr(input: &str) -> IResult<&str, Sexpr> {
    map(
        delimited(
            tag("("),
            many0(map(expr, Arc::new)),
            preceded(opt(ws), tag(")")),
        ),
        Sexpr,
    )(input)
}

/// Parse a natural number literal
///
/// A natural number literal is either
/// - A sequence of decimal digits, e.g. `00120013`
/// - A sequence of hexadecimal digits prefixed by `0x`, e.g. `0xABC`
/// - A sequence of octal digits prefixed by `0o`, e.g. `0o163`
/// - A sequence of binary digits prefixed by `0b`, e.g. `0b1101`
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::natural;
/// assert_eq!(natural("0123hello"), Ok(("hello", 123u32.into())));
/// assert_eq!(natural("0xABCH"), Ok(("H", 0xABCu32.into())));
/// assert_eq!(natural("0o129"), Ok(("9", 0o12u32.into())));
/// assert_eq!(natural("0b0111012"), Ok(("2", 0b011101u32.into())));
/// assert_eq!(natural("0b2"), Ok(("b2", 0u32.into())));
/// ```
pub fn natural(input: &str) -> IResult<&str, BigUint> {
    alt((
        map_opt(preceded(tag("0x"), hex_digit1), |digits: &str| {
            BigUint::parse_bytes(digits.as_bytes(), 16)
        }),
        map_opt(preceded(tag("0o"), oct_digit1), |digits: &str| {
            BigUint::parse_bytes(digits.as_bytes(), 8)
        }),
        map_opt(preceded(tag("0b"), is_a("01")), |digits: &str| {
            BigUint::parse_bytes(digits.as_bytes(), 2)
        }),
        map_opt(digit1, |digits: &str| {
            BigUint::parse_bytes(digits.as_bytes(), 10)
        }),
    ))(input)
}
/// Parse a boolean variable
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::boolean;
/// assert_eq!(boolean("#true"), Ok(("", true)));
/// assert_eq!(boolean("#false"), Ok(("", false)));
/// ```
pub fn boolean(input: &str) -> IResult<&str, bool> {
    alt((map(tag(TRUE), |_| true), map(tag(FALSE), |_| false)))(input)
}

/// Parse a polymorphic universe
/// 
/// # Examples
/// ```rust
/// # use rain_ast::parser::universe;
/// # use rain_ast::ast::Expr;
/// use Expr::Universe;
/// assert_eq!(universe("#universe"), Ok(("", Universe(0))));
/// assert_eq!(universe("#universe[4]"), Ok(("", Universe(4))));
/// assert_eq!(universe("#universe[ 321]"), Ok(("", Universe(321))));
/// ```
pub fn universe(input: &str) -> IResult<&str, Expr> {
    preceded(
        tag(UNIVERSE),
        map(
            opt(delimited(
                preceded(tag("["), opt(ws)),
                u64_dec,
                preceded(opt(ws), tag("]")),
            )),
            |level| Expr::Universe(level.unwrap_or(0)),
        ),
    )(input)
}
