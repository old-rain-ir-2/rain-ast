/*!
The AST for the `rain` intermediate representation
*/
use num::BigUint;
use std::sync::Arc;

/// A `rain` expression: this is the root of most `rain` ASTs
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Expr {
    /// An identifier
    Ident(String),
    /// An S-expression
    Sexpr(Sexpr),
    /// A natural number literal
    Natural(BigUint),
    /// The type of natural numbers
    Nats,
    /// The family of finite types
    Finite,
    /// A boolean constant
    Boolean(bool),
    /// The type of booleans
    Bool,
    /// A leveled polymorphic typing universe
    Universe(u64),
}

/// An S-expression
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Sexpr(pub Vec<Arc<Expr>>);

impl Sexpr {
    /// The empty S-expression
    pub const NIL: Sexpr = Sexpr(Vec::new());
}
